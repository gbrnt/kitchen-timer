$fn = 48;


difference(){
	difference() {
		union() {
			translate(v = [0, 0, -1.5000000000]) {
				linear_extrude(height = 2.5000000000) {
					offset(r = 2) {
						square(center = true, size = [13.0000000000, 13.0000000000]);
					}
				}
			}
			translate(v = [0, 0, -5.5000000000]) {
				difference() {
					translate(v = [0, 0, 2.0000000000]) {
						union() {
							cube(center = true, size = [7, 5, 4]);
							cylinder(center = true, d = 5.5000000000, h = 4);
						}
					}
					translate(v = [0, 0, -0.0100000000]) {
					}
				}
			}
		}
		translate(v = [0, 0, 1.0100000000]) {
			translate(v = [0, 0, -0.3000000000]) {
				linear_extrude(height = 0.3000000000) {
					text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "M+", valign = "center");
				}
			}
		}
	}
	/* Holes Below*/
	union(){
		union(){
			translate(v = [0, 0, -5.5000000000]){
				union(){
					translate(v = [0, 0, -0.0100000000]){
						union() {
							linear_extrude(height = 4) {
								union() {
									square(center = true, size = [1.1500000000, 4.2000000000]);
									square(center = true, size = [4.2000000000, 1.3000000000]);
								}
							}
							union() {
								hull() {
									translate(v = [0, 1.5250000000, 0]) {
										linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
											square(center = true, size = [1.7500000000, 1.7500000000]);
										}
									}
									translate(v = [0, -1.5250000000, 0]) {
										linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
											square(center = true, size = [1.7500000000, 1.7500000000]);
										}
									}
								}
								hull() {
									translate(v = [1.4500000000, 0, 0]) {
										linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
											square(center = true, size = [1.9000000000, 1.9000000000]);
										}
									}
									translate(v = [-1.4500000000, 0, 0]) {
										linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
											square(center = true, size = [1.9000000000, 1.9000000000]);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	} /* End Holes */ 
}