#!/bin/bash

# Export all the kitchen timer's .scad files (except assembly) to .stl for 3D printing

mkdir -p export/caps

for f in $(ls *.scad | grep -v assembly);
do
    part_name=$(echo $f | cut -f 1 -d ".");
    echo openscad -o export/$part_name.stl $f
    openscad -o export/$part_name.stl $f
done;

for f in caps/*.scad;
do
    part_name=$(echo $f | cut -f 1 -d ".");
    echo openscad -o export/$part_name.stl $f
    openscad -o export/$part_name.stl $f
done;
