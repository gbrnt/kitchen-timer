"""
Generate case for kitchen timer
2021 George Bryant
"""

# SolidPython generates OpenSCAD code, allowing me to write less verbose Python
from solid import (
    translate, union, linear_extrude, hole, part, hull,
    cube, cylinder, sphere,
    square, offset, text,
    color, scad_render_to_file)
from solid.utils import up, down

SEGMENTS = 48

# Useful dimensions in millimetres
d_width = 120            # Overall width of the timer
d_height = 90            # Overall height of the timer (not thickness)
d_corner_radius = 4      # Radius of corners (but not top/bottom edges)
d_base_thickness = 12    # Overall thickness of base
d_base_material_t = 2    # Material thickness of base
d_top_thickness = 17     # Overall thickness of the top
d_key_hole_width = 14.2  # Size of the hole in the plate
d_key_top_width = 18     # Size of the hole above the plate
d_key_bottom_width = 18  # Size of the hole below the plate
d_key_unit = 20          # Spacing between keys
d_plate_thickness = 1.5  # Thickness of plate itself
d_plate_depth = 12       # How far down plate is set from top surface
d_insert_edge = 5   # Distance from hole centre to edge
d_insert_dia = 4.2  # Diameter of hole for insert
d_screw_cbore_dia = 7.5  # Screw counterbore diameter
d_screw_cbore = 5        # Screw counterbore depth
d_screw_hole_dia = 3.5   # Diameter of screw hole
d_smol = 0.01            # Tiny amount to add to prevent collisions


def build_top(outline, keys):
    """
    Return top of timer
    """

    key_cutter = build_key_cutter()

    top_shape = linear_extrude(height=d_top_thickness)(outline)

    # Iterate through keys and cut out of top
    for key in keys:
        top_shape -= translate([key.xpos_mm, key.ypos_mm, 0])(key_cutter)

    # Cut hole for display cover, display and wires out of top, then hollow space out underneath
    d_screen_cover = [58, 38, 0.4+d_smol]
    d_screen = [40, 13, 4]
    d_screen_wires = [5, 13, d_top_thickness+2*d_smol]
    d_below_screen = [58, 38, d_top_thickness-d_screen_cover[2]-d_screen[2]-2+d_smol]
    top_shape -= translate([d_width/2, d_height/2, d_top_thickness-d_screen_cover[2]/2+d_smol])(
        cube(d_screen_cover, center=True))
    top_shape -= translate([d_width/2, d_height/2, d_top_thickness-(d_screen[2]+d_screen_cover[2])/2])(
        cube(d_screen, center=True))
    top_shape -= translate([d_width/2+d_screen[0]/2-d_screen_wires[0]/2, d_height/2, d_screen_wires[2]/2-d_smol])(
        cube(d_screen_wires, center=True))
    top_shape -= translate([d_width/2, d_height/2, d_below_screen[2]/2-d_smol])(
        cube(d_below_screen, center=True))

    # Cut holes for inserts
    insert_cutter = cylinder(d=d_insert_dia, h=d_top_thickness-2+d_smol)
    top_shape -= translate([d_insert_edge, d_insert_edge, -d_smol])(insert_cutter)
    top_shape -= translate([d_width-d_insert_edge, d_insert_edge, -d_smol])(insert_cutter)
    top_shape -= translate([d_insert_edge, d_height-d_insert_edge, -d_smol])(insert_cutter)
    top_shape -= translate([d_width-d_insert_edge, d_height-d_insert_edge, -d_smol])(insert_cutter)

    return top_shape


def build_base(outline):
    """
    Return base of timer
    """

    # Main shape
    base_shape_outline = down(d_base_thickness)(linear_extrude(height=d_base_thickness)(outline))
    base_shape = base_shape_outline

    # Hollow most of base
    base_shape -= translate([d_base_material_t, d_base_material_t, -d_base_thickness+d_base_material_t])(
        cube([d_width-2*d_base_material_t, d_height-2*d_base_material_t, d_base_thickness-d_base_material_t+d_smol]))

    # Add mounting hole bosses and then remove mounting holes
    hole_locations = [
        [d_insert_edge, d_insert_edge, 0],
        [d_width-d_insert_edge, d_insert_edge, 0],
        [d_insert_edge, d_height-d_insert_edge, 0],
        [d_width-d_insert_edge, d_height-d_insert_edge, 0]
    ]
    hole_boss = down(d_base_thickness)(cylinder(d=d_screw_cbore_dia+2*d_base_material_t, h=d_base_thickness))
    hole_cutter = down(d_base_thickness+d_smol)(union()(
        cylinder(d=d_screw_hole_dia, h=d_base_thickness+2*d_smol),
        cylinder(d=d_screw_cbore_dia, h=d_screw_cbore+d_smol)
    ))
    for loc in hole_locations:
        base_shape += translate(loc)(hole_boss)
        base_shape -= translate(loc)(hole_cutter)

    # Trim outline so hole bosses aren't sticking out at the corners
    base_shape *= base_shape_outline

    # Mount for passive buzzer
    d_buzzer_dia = 17      # Diameter of case of buzzer
    d_buzzer_hole_dia = 4  # The hole the sound comes out of
    d_buzzer_t = 4         # Thickness of buzzer
    d_buzzer_mount_t = 2   # Thickness of walls of buzzer mount
    # Outer cylinder
    buzzer_mount = cylinder(d=d_buzzer_dia+2*d_buzzer_mount_t, h=d_buzzer_t)
    # Hole for buzzer
    buzzer_mount -= cylinder(d=d_buzzer_dia, h=d_buzzer_t+d_smol)
    # Add notch for cables to exit
    buzzer_mount -= translate([-d_buzzer_dia/2-d_buzzer_mount_t, -2, 0])((
        cube([d_buzzer_dia+2*d_buzzer_mount_t, 4, d_buzzer_t+d_smol])))
    # Hole for sound
    buzzer_mount -= hole()(down(d_smol+d_base_material_t)(
        cylinder(d=d_buzzer_hole_dia, h=d_buzzer_t+d_base_material_t+2*d_smol)))
    buzzer_mount -= hole()(translate([-d_buzzer_hole_dia/2, -d_buzzer_dia, -d_base_material_t-d_smol])(
        cube([d_buzzer_hole_dia, d_buzzer_dia, d_base_material_t/2])))
    # Translate to correct position
    buzzer_mount = translate([d_width/2, d_buzzer_dia/2+d_base_material_t, -d_base_thickness+d_base_material_t])(buzzer_mount)
    base_shape += buzzer_mount

    # USB charger mount
    d_charger_pcb = [17.5, 28, 1.6]  # XYZ for the PCB itself
    d_charger_bottom_clearance = 2   # Clearance for wires sticking out the bottom
    d_usbc = [10, 7.5, 3.5]          # The USB C port
    d_usb_stickout = 1               # How far the port sticks out past the PCB edge
    d_charger_mount_t = 2            # Thickness of the bits surrounding the PCB
    # Main base shape
    charger_mount = translate([-d_charger_pcb[0]/2-d_charger_mount_t, -d_charger_pcb[1]-d_charger_mount_t, 0])(cube([
        d_charger_pcb[0]+2*d_charger_mount_t,
        d_charger_pcb[1]+d_charger_mount_t,
        d_charger_bottom_clearance+d_charger_pcb[2]]))
    # Cut recess for PCB
    charger_mount -= translate([-d_charger_pcb[0]/2, -d_charger_pcb[1], d_charger_bottom_clearance])(
        cube([d_charger_pcb[0], d_charger_pcb[1], d_charger_pcb[2]+d_smol]))
    # Cut extra recess for wires
    charger_mount -= translate([-d_charger_pcb[0]/2, -d_charger_pcb[1], 0])(
        cube([d_charger_pcb[0], 5, d_charger_bottom_clearance+d_charger_pcb[2]+d_smol]))
    charger_mount -= translate([-12.5/2, -d_charger_pcb[1]-d_charger_mount_t-d_smol, 0])(
        cube([12.5, d_charger_mount_t+2*d_smol, d_charger_bottom_clearance+d_charger_pcb[2]+d_smol]))
    # Cut hole for USB port
    charger_mount -= hole()(translate([-d_usbc[0]/2, -d_smol, d_charger_bottom_clearance+d_charger_pcb[2]])(cube(d_usbc)))

    base_shape += translate([25, d_height-d_base_material_t, -d_base_thickness+d_base_material_t])(charger_mount)

    return base_shape


def build_key_cutter():
    """
    Return cutter for one key
    """
    d_below_plate = d_top_thickness - d_plate_thickness - d_plate_depth

    cutter_top = cube([d_key_top_width, d_key_top_width, d_plate_depth+2*d_smol], center=True)
    cutter_top = up(d_top_thickness-d_plate_depth/2+d_smol)(cutter_top)

    cutter_plate = cube([d_key_hole_width, d_key_hole_width, d_top_thickness], center=True)
    cutter_plate = up(d_top_thickness/2)(cutter_plate)

    cutter_bottom = cube([d_key_bottom_width, d_key_bottom_width, d_below_plate+2*d_smol], center=True)
    cutter_bottom = up(d_below_plate/2-d_smol)(cutter_bottom)

    return hole()(cutter_top + cutter_plate + cutter_bottom)


class Key(object):
    def __init__(self, name, xpos, ypos, legend_size=5):
        self.name = name
        self.legend_size = legend_size
        # X and Y positions in key units
        self.xpos_units = xpos
        self.ypos_units = ypos

    def calculate_position(self, centre_x, centre_y):
        """
        Convert position in "key units" relative to a centre position to mm relative to origin
        """
        self.xpos_mm = self.xpos_units * d_key_unit + centre_x
        self.ypos_mm = self.ypos_units * d_key_unit + centre_y

    def add_cap(self, cap):
        """
        Store a keycap in the key object
        """
        self.cap = cap


def build_keycap(legend, legend_size=5):
    """
    Return one keycap with legend engraved into top
    """
    d_switch_height = 6.5    # Height of the top of an MX switch from the top of the plate
    d_switch_travel = 4      # Total distance the switch plunger moves
    d_switch_stem_height = 4 # Height of switch stem above switch top
    d_cap_clearance = 0.5    # Clearance between keycap and case on each side
    d_cap_stickout = 1       # Amount cap sticks out above top
    d_cap_corner_radius = 2  # Radius of the side edges, not the top
    d_cap_width = d_key_top_width - 2*d_cap_clearance
    d_cap_top_height = d_plate_depth - d_switch_height - d_switch_travel + d_cap_stickout

    cap_top_outline = offset(d_cap_corner_radius)(
        square([d_cap_width-d_cap_corner_radius*2, d_cap_width-d_cap_corner_radius*2], center=True))

    cap_top = down(d_cap_top_height-d_cap_stickout)(
        linear_extrude(height=d_cap_top_height)(cap_top_outline))

    # Make the stem of the cap
    d_stem_x = 7
    d_stem_y = 5
    d_stem_circle_dia = 5.5

    stem = cube([d_stem_x, d_stem_y, d_switch_stem_height], center=True)
    stem += cylinder(d=d_stem_circle_dia, h=d_switch_stem_height, center=True)
    stem = up(d_switch_stem_height/2)(stem)

    # Make a cutter to cut the cross-shaped hole for the switch's stem
    d_cross_length = 4.2
    d_cross_width_x = 1.15
    d_cross_width_y = 1.30
    d_cross_chamfer = 0.3

    cross_outline = union()(
        square([d_cross_width_x, d_cross_length], center=True),
        square([d_cross_length, d_cross_width_y], center=True))
    cross_cutter = linear_extrude(d_switch_stem_height)(cross_outline)

    # Add a chamfer to the entrance of the cross hole
    chamfer_tool = linear_extrude(d_cross_chamfer,
            scale=1-(2*d_cross_chamfer/d_cross_width_x))(square(
        [d_cross_width_x+2*d_cross_chamfer, d_cross_width_x+2*d_cross_chamfer],
        center=True))
    chamfer_x = hull()(
        translate([0, (d_cross_length-d_cross_width_x)/2, 0])(chamfer_tool),
        translate([0, -(d_cross_length-d_cross_width_x)/2, 0])(chamfer_tool),
    )
    chamfer_tool = linear_extrude(d_cross_chamfer,
            scale=1-(2*d_cross_chamfer/d_cross_width_y))(square(
        [d_cross_width_y+2*d_cross_chamfer, d_cross_width_y+2*d_cross_chamfer],
        center=True))
    chamfer_y = hull()(
        translate([(d_cross_length-d_cross_width_y)/2, 0, 0])(chamfer_tool),
        translate([-(d_cross_length-d_cross_width_y)/2, 0, 0])(chamfer_tool),
    )
    cross_cutter += chamfer_x + chamfer_y
    stem -= down(d_smol)(hole()(cross_cutter))

    # Create the legend
    d_legend_depth = 0.3
    leg = down(d_legend_depth)(
        linear_extrude(d_legend_depth)(
            text(legend, size=legend_size, font="Noto Sans:style=Black",
                halign="center", valign="center")))
    leg = up(d_cap_stickout+d_smol)(leg) # Move up to top of cap

    # Add the stem to the top, remove the legend
    cap = cap_top + down(d_switch_stem_height+d_cap_top_height-d_cap_stickout)(stem)
    cap -= leg

    return cap


if __name__ == "__main__":
    # Create outline of both base and top as a 2D shape
    case_outline = translate([d_corner_radius, d_corner_radius, 0])(
        offset(d_corner_radius)(
            square([d_width-d_corner_radius*2, d_height-d_corner_radius*2], center=False)
        )
    )

    # Position keys relative to origin, then adjust to be relative to centre of top
    # Key positions are in "key units" - ie multiples of d_key_unit
    keys = [
        Key("A", -2, 1.5),  Key("H+", -1, 1.5),  Key("M+", 0, 1.5),  Key("S+", 1, 1.5),  Key("B", 2, 1.5),
        Key("T1", -2, 0.5),                                                             Key("RESET", 2, 0.5, 3),
        Key("T2", -2, -0.5),                                                     Key("START", 2, -0.5, 3),
        Key("C", -2, -1.5), Key("H-", -1, -1.5), Key("M-", 0, -1.5), Key("S-", 1, -1.5), Key("D", 2, -1.5),
    ]
    for key in keys:
        key.calculate_position(d_width/2, d_height/2)
        key.add_cap(build_keycap(key.name, key.legend_size))

    # Build top and base
    top = part()(color("grey")(build_top(case_outline, keys)))
    base = part()(color("blue")(build_base(case_outline)))

    # Build caps and add to assembly
    assembly = top + base
    for key in keys:
        scad_render_to_file(key.cap, f"caps/key-{key.name}.scad",
            file_header=f"$fn = {SEGMENTS};", include_orig_code=False)
        assembly += part()(
            translate([key.xpos_mm, key.ypos_mm, d_top_thickness])(
                key.cap))

    scad_render_to_file(top, "timer-case-top.scad", file_header=f"$fn = {SEGMENTS};", include_orig_code=False)
    scad_render_to_file(base, "timer-case-base.scad", file_header=f"$fn = {SEGMENTS};", include_orig_code=False)
    scad_render_to_file(assembly, "timer-case-assembly.scad", file_header=f"$fn = {SEGMENTS};", include_orig_code=False)
