$fn = 48;


union() {
	difference(){
		color(alpha = 1.0000000000, c = "grey") {
			difference() {
				linear_extrude(height = 17) {
					translate(v = [4, 4, 0]) {
						offset(r = 4) {
							square(center = false, size = [112, 82]);
						}
					}
				}
				translate(v = [20.0000000000, 75.0000000000, 0]) {
				}
				translate(v = [40.0000000000, 75.0000000000, 0]) {
				}
				translate(v = [60.0000000000, 75.0000000000, 0]) {
				}
				translate(v = [80.0000000000, 75.0000000000, 0]) {
				}
				translate(v = [100.0000000000, 75.0000000000, 0]) {
				}
				translate(v = [20.0000000000, 55.0000000000, 0]) {
				}
				translate(v = [100.0000000000, 55.0000000000, 0]) {
				}
				translate(v = [20.0000000000, 35.0000000000, 0]) {
				}
				translate(v = [100.0000000000, 35.0000000000, 0]) {
				}
				translate(v = [20.0000000000, 15.0000000000, 0]) {
				}
				translate(v = [40.0000000000, 15.0000000000, 0]) {
				}
				translate(v = [60.0000000000, 15.0000000000, 0]) {
				}
				translate(v = [80.0000000000, 15.0000000000, 0]) {
				}
				translate(v = [100.0000000000, 15.0000000000, 0]) {
				}
				translate(v = [60.0000000000, 45.0000000000, 16.8050000000]) {
					cube(center = true, size = [58, 38, 0.4100000000]);
				}
				translate(v = [60.0000000000, 45.0000000000, 14.7950000000]) {
					cube(center = true, size = [40, 13, 4]);
				}
				translate(v = [77.5000000000, 45.0000000000, 8.5000000000]) {
					cube(center = true, size = [5, 13, 17.0200000000]);
				}
				translate(v = [60.0000000000, 45.0000000000, 5.2900000000]) {
					cube(center = true, size = [58, 38, 10.6000000000]);
				}
				translate(v = [5, 5, -0.0100000000]) {
					cylinder(d = 4.2000000000, h = 15.0100000000);
				}
				translate(v = [115, 5, -0.0100000000]) {
					cylinder(d = 4.2000000000, h = 15.0100000000);
				}
				translate(v = [5, 85, -0.0100000000]) {
					cylinder(d = 4.2000000000, h = 15.0100000000);
				}
				translate(v = [115, 85, -0.0100000000]) {
					cylinder(d = 4.2000000000, h = 15.0100000000);
				}
			}
		}
		/* Holes Below*/
		color(alpha = 1.0000000000, c = "grey"){
			union(){
				translate(v = [20.0000000000, 75.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [40.0000000000, 75.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [60.0000000000, 75.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [80.0000000000, 75.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [100.0000000000, 75.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [20.0000000000, 55.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [100.0000000000, 55.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [20.0000000000, 35.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [100.0000000000, 35.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [20.0000000000, 15.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [40.0000000000, 15.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [60.0000000000, 15.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [80.0000000000, 15.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
				translate(v = [100.0000000000, 15.0000000000, 0]){
					union() {
						translate(v = [0, 0, 11.0100000000]) {
							cube(center = true, size = [18, 18, 12.0200000000]);
						}
						translate(v = [0, 0, 8.5000000000]) {
							cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
						}
						translate(v = [0, 0, 1.7400000000]) {
							cube(center = true, size = [18, 18, 3.5200000000]);
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		color(alpha = 1.0000000000, c = "blue") {
			union() {
				intersection() {
					difference() {
						union() {
							difference() {
								union() {
									difference() {
										union() {
											difference() {
												union() {
													difference() {
														translate(v = [0, 0, -12]) {
															linear_extrude(height = 12) {
																translate(v = [4, 4, 0]) {
																	offset(r = 4) {
																		square(center = false, size = [112, 82]);
																	}
																}
															}
														}
														translate(v = [2, 2, -10]) {
															cube(size = [116, 86, 10.0100000000]);
														}
													}
													translate(v = [5, 5, 0]) {
														translate(v = [0, 0, -12]) {
															cylinder(d = 11.5000000000, h = 12);
														}
													}
												}
												translate(v = [5, 5, 0]) {
													translate(v = [0, 0, -12.0100000000]) {
														union() {
															cylinder(d = 3.5000000000, h = 12.0200000000);
															cylinder(d = 7.5000000000, h = 5.0100000000);
														}
													}
												}
											}
											translate(v = [115, 5, 0]) {
												translate(v = [0, 0, -12]) {
													cylinder(d = 11.5000000000, h = 12);
												}
											}
										}
										translate(v = [115, 5, 0]) {
											translate(v = [0, 0, -12.0100000000]) {
												union() {
													cylinder(d = 3.5000000000, h = 12.0200000000);
													cylinder(d = 7.5000000000, h = 5.0100000000);
												}
											}
										}
									}
									translate(v = [5, 85, 0]) {
										translate(v = [0, 0, -12]) {
											cylinder(d = 11.5000000000, h = 12);
										}
									}
								}
								translate(v = [5, 85, 0]) {
									translate(v = [0, 0, -12.0100000000]) {
										union() {
											cylinder(d = 3.5000000000, h = 12.0200000000);
											cylinder(d = 7.5000000000, h = 5.0100000000);
										}
									}
								}
							}
							translate(v = [115, 85, 0]) {
								translate(v = [0, 0, -12]) {
									cylinder(d = 11.5000000000, h = 12);
								}
							}
						}
						translate(v = [115, 85, 0]) {
							translate(v = [0, 0, -12.0100000000]) {
								union() {
									cylinder(d = 3.5000000000, h = 12.0200000000);
									cylinder(d = 7.5000000000, h = 5.0100000000);
								}
							}
						}
					}
					translate(v = [0, 0, -12]) {
						linear_extrude(height = 12) {
							translate(v = [4, 4, 0]) {
								offset(r = 4) {
									square(center = false, size = [112, 82]);
								}
							}
						}
					}
				}
				translate(v = [60.0000000000, 10.5000000000, -10]) {
					difference() {
						cylinder(d = 21, h = 4);
						cylinder(d = 17, h = 4.0100000000);
						translate(v = [-10.5000000000, -2, 0]) {
							cube(size = [21, 4, 4.0100000000]);
						}
					}
				}
				translate(v = [25, 88, -10]) {
					difference() {
						translate(v = [-10.7500000000, -30, 0]) {
							cube(size = [21.5000000000, 30, 3.6000000000]);
						}
						translate(v = [-8.7500000000, -28, 2]) {
							cube(size = [17.5000000000, 28, 1.6100000000]);
						}
						translate(v = [-8.7500000000, -28, 0]) {
							cube(size = [17.5000000000, 5, 3.6100000000]);
						}
						translate(v = [-6.2500000000, -30.0100000000, 0]) {
							cube(size = [12.5000000000, 2.0200000000, 3.6100000000]);
						}
					}
				}
			}
		}
		/* Holes Below*/
		color(alpha = 1.0000000000, c = "blue"){
			union(){
				translate(v = [60.0000000000, 10.5000000000, -10]){
					union(){
						translate(v = [0, 0, -2.0100000000]) {
							cylinder(d = 4, h = 6.0200000000);
						}
						translate(v = [-2.0000000000, -17, -2.0100000000]) {
							cube(size = [4, 17, 1.0000000000]);
						}
					}
				}
				translate(v = [25, 88, -10]){
					union(){
						translate(v = [-5.0000000000, -0.0100000000, 3.6000000000]) {
							cube(size = [10, 7.5000000000, 3.5000000000]);
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [20.0000000000, 75.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "A", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [20.0000000000, 75.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [40.0000000000, 75.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "H+", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [40.0000000000, 75.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [60.0000000000, 75.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "M+", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [60.0000000000, 75.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [80.0000000000, 75.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "S+", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [80.0000000000, 75.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [100.0000000000, 75.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "B", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [100.0000000000, 75.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [20.0000000000, 55.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "T1", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [20.0000000000, 55.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [100.0000000000, 55.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 3, text = "RESET", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [100.0000000000, 55.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [20.0000000000, 35.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "T2", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [20.0000000000, 35.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [100.0000000000, 35.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 3, text = "START", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [100.0000000000, 35.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [20.0000000000, 15.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "C", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [20.0000000000, 15.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [40.0000000000, 15.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "H-", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [40.0000000000, 15.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [60.0000000000, 15.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "M-", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [60.0000000000, 15.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [80.0000000000, 15.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "S-", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [80.0000000000, 15.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
	difference(){
		translate(v = [100.0000000000, 15.0000000000, 17]) {
			difference() {
				union() {
					translate(v = [0, 0, -1.5000000000]) {
						linear_extrude(height = 2.5000000000) {
							offset(r = 2) {
								square(center = true, size = [13.0000000000, 13.0000000000]);
							}
						}
					}
					translate(v = [0, 0, -5.5000000000]) {
						difference() {
							translate(v = [0, 0, 2.0000000000]) {
								union() {
									cube(center = true, size = [7, 5, 4]);
									cylinder(center = true, d = 5.5000000000, h = 4);
								}
							}
							translate(v = [0, 0, -0.0100000000]) {
							}
						}
					}
				}
				translate(v = [0, 0, 1.0100000000]) {
					translate(v = [0, 0, -0.3000000000]) {
						linear_extrude(height = 0.3000000000) {
							text(font = "Noto Sans:style=Black", halign = "center", size = 5, text = "D", valign = "center");
						}
					}
				}
			}
		}
		/* Holes Below*/
		translate(v = [100.0000000000, 15.0000000000, 17]){
			union(){
				union(){
					translate(v = [0, 0, -5.5000000000]){
						union(){
							translate(v = [0, 0, -0.0100000000]){
								union() {
									linear_extrude(height = 4) {
										union() {
											square(center = true, size = [1.1500000000, 4.2000000000]);
											square(center = true, size = [4.2000000000, 1.3000000000]);
										}
									}
									union() {
										hull() {
											translate(v = [0, 1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
											translate(v = [0, -1.5250000000, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.4782608696) {
													square(center = true, size = [1.7500000000, 1.7500000000]);
												}
											}
										}
										hull() {
											translate(v = [1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
											translate(v = [-1.4500000000, 0, 0]) {
												linear_extrude(height = 0.3000000000, scale = 0.5384615385) {
													square(center = true, size = [1.9000000000, 1.9000000000]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} /* End Holes */ 
	}
}