$fn = 48;


difference(){
	color(alpha = 1.0000000000, c = "blue") {
		union() {
			intersection() {
				difference() {
					union() {
						difference() {
							union() {
								difference() {
									union() {
										difference() {
											union() {
												difference() {
													translate(v = [0, 0, -12]) {
														linear_extrude(height = 12) {
															translate(v = [4, 4, 0]) {
																offset(r = 4) {
																	square(center = false, size = [112, 82]);
																}
															}
														}
													}
													translate(v = [2, 2, -10]) {
														cube(size = [116, 86, 10.0100000000]);
													}
												}
												translate(v = [5, 5, 0]) {
													translate(v = [0, 0, -12]) {
														cylinder(d = 11.5000000000, h = 12);
													}
												}
											}
											translate(v = [5, 5, 0]) {
												translate(v = [0, 0, -12.0100000000]) {
													union() {
														cylinder(d = 3.5000000000, h = 12.0200000000);
														cylinder(d = 7.5000000000, h = 5.0100000000);
													}
												}
											}
										}
										translate(v = [115, 5, 0]) {
											translate(v = [0, 0, -12]) {
												cylinder(d = 11.5000000000, h = 12);
											}
										}
									}
									translate(v = [115, 5, 0]) {
										translate(v = [0, 0, -12.0100000000]) {
											union() {
												cylinder(d = 3.5000000000, h = 12.0200000000);
												cylinder(d = 7.5000000000, h = 5.0100000000);
											}
										}
									}
								}
								translate(v = [5, 85, 0]) {
									translate(v = [0, 0, -12]) {
										cylinder(d = 11.5000000000, h = 12);
									}
								}
							}
							translate(v = [5, 85, 0]) {
								translate(v = [0, 0, -12.0100000000]) {
									union() {
										cylinder(d = 3.5000000000, h = 12.0200000000);
										cylinder(d = 7.5000000000, h = 5.0100000000);
									}
								}
							}
						}
						translate(v = [115, 85, 0]) {
							translate(v = [0, 0, -12]) {
								cylinder(d = 11.5000000000, h = 12);
							}
						}
					}
					translate(v = [115, 85, 0]) {
						translate(v = [0, 0, -12.0100000000]) {
							union() {
								cylinder(d = 3.5000000000, h = 12.0200000000);
								cylinder(d = 7.5000000000, h = 5.0100000000);
							}
						}
					}
				}
				translate(v = [0, 0, -12]) {
					linear_extrude(height = 12) {
						translate(v = [4, 4, 0]) {
							offset(r = 4) {
								square(center = false, size = [112, 82]);
							}
						}
					}
				}
			}
			translate(v = [60.0000000000, 10.5000000000, -10]) {
				difference() {
					cylinder(d = 21, h = 4);
					cylinder(d = 17, h = 4.0100000000);
					translate(v = [-10.5000000000, -2, 0]) {
						cube(size = [21, 4, 4.0100000000]);
					}
				}
			}
			translate(v = [25, 88, -10]) {
				difference() {
					translate(v = [-10.7500000000, -30, 0]) {
						cube(size = [21.5000000000, 30, 3.6000000000]);
					}
					translate(v = [-8.7500000000, -28, 2]) {
						cube(size = [17.5000000000, 28, 1.6100000000]);
					}
					translate(v = [-8.7500000000, -28, 0]) {
						cube(size = [17.5000000000, 5, 3.6100000000]);
					}
					translate(v = [-6.2500000000, -30.0100000000, 0]) {
						cube(size = [12.5000000000, 2.0200000000, 3.6100000000]);
					}
				}
			}
		}
	}
	/* Holes Below*/
	color(alpha = 1.0000000000, c = "blue"){
		union(){
			translate(v = [60.0000000000, 10.5000000000, -10]){
				union(){
					translate(v = [0, 0, -2.0100000000]) {
						cylinder(d = 4, h = 6.0200000000);
					}
					translate(v = [-2.0000000000, -17, -2.0100000000]) {
						cube(size = [4, 17, 1.0000000000]);
					}
				}
			}
			translate(v = [25, 88, -10]){
				union(){
					translate(v = [-5.0000000000, -0.0100000000, 3.6000000000]) {
						cube(size = [10, 7.5000000000, 3.5000000000]);
					}
				}
			}
		}
	} /* End Holes */ 
}