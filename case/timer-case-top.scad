$fn = 48;


difference(){
	color(alpha = 1.0000000000, c = "grey") {
		difference() {
			linear_extrude(height = 17) {
				translate(v = [4, 4, 0]) {
					offset(r = 4) {
						square(center = false, size = [112, 82]);
					}
				}
			}
			translate(v = [20.0000000000, 75.0000000000, 0]) {
			}
			translate(v = [40.0000000000, 75.0000000000, 0]) {
			}
			translate(v = [60.0000000000, 75.0000000000, 0]) {
			}
			translate(v = [80.0000000000, 75.0000000000, 0]) {
			}
			translate(v = [100.0000000000, 75.0000000000, 0]) {
			}
			translate(v = [20.0000000000, 55.0000000000, 0]) {
			}
			translate(v = [100.0000000000, 55.0000000000, 0]) {
			}
			translate(v = [20.0000000000, 35.0000000000, 0]) {
			}
			translate(v = [100.0000000000, 35.0000000000, 0]) {
			}
			translate(v = [20.0000000000, 15.0000000000, 0]) {
			}
			translate(v = [40.0000000000, 15.0000000000, 0]) {
			}
			translate(v = [60.0000000000, 15.0000000000, 0]) {
			}
			translate(v = [80.0000000000, 15.0000000000, 0]) {
			}
			translate(v = [100.0000000000, 15.0000000000, 0]) {
			}
			translate(v = [60.0000000000, 45.0000000000, 16.8050000000]) {
				cube(center = true, size = [58, 38, 0.4100000000]);
			}
			translate(v = [60.0000000000, 45.0000000000, 14.7950000000]) {
				cube(center = true, size = [40, 13, 4]);
			}
			translate(v = [77.5000000000, 45.0000000000, 8.5000000000]) {
				cube(center = true, size = [5, 13, 17.0200000000]);
			}
			translate(v = [60.0000000000, 45.0000000000, 5.2900000000]) {
				cube(center = true, size = [58, 38, 10.6000000000]);
			}
			translate(v = [5, 5, -0.0100000000]) {
				cylinder(d = 4.2000000000, h = 15.0100000000);
			}
			translate(v = [115, 5, -0.0100000000]) {
				cylinder(d = 4.2000000000, h = 15.0100000000);
			}
			translate(v = [5, 85, -0.0100000000]) {
				cylinder(d = 4.2000000000, h = 15.0100000000);
			}
			translate(v = [115, 85, -0.0100000000]) {
				cylinder(d = 4.2000000000, h = 15.0100000000);
			}
		}
	}
	/* Holes Below*/
	color(alpha = 1.0000000000, c = "grey"){
		union(){
			translate(v = [20.0000000000, 75.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [40.0000000000, 75.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [60.0000000000, 75.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [80.0000000000, 75.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [100.0000000000, 75.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [20.0000000000, 55.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [100.0000000000, 55.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [20.0000000000, 35.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [100.0000000000, 35.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [20.0000000000, 15.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [40.0000000000, 15.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [60.0000000000, 15.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [80.0000000000, 15.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
			translate(v = [100.0000000000, 15.0000000000, 0]){
				union() {
					translate(v = [0, 0, 11.0100000000]) {
						cube(center = true, size = [18, 18, 12.0200000000]);
					}
					translate(v = [0, 0, 8.5000000000]) {
						cube(center = true, size = [14.2000000000, 14.2000000000, 17]);
					}
					translate(v = [0, 0, 1.7400000000]) {
						cube(center = true, size = [18, 18, 3.5200000000]);
					}
				}
			}
		}
	} /* End Holes */ 
}