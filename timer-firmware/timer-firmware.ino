/*
 * Kitchen timer - with OLED and physical buttons
 */

#include "U8g2lib.h"
#include <EEPROM.h>

enum button_pins {
	BUTTON_A=14,
	BUTTON_B=20,
	BUTTON_C=9,
	BUTTON_D=0,
	BUTTON_H_UP=15,
	BUTTON_H_DN=8,
	BUTTON_M_UP=18,
	BUTTON_M_DN=7,
	BUTTON_S_UP=19,
	BUTTON_S_DN=6,
	BUTTON_RESET=21,
	BUTTON_STARTSTOP=4,
	BUTTON_TIMER1=16,
	BUTTON_TIMER2=10,
};
const uint8_t buttons[] = {BUTTON_A, BUTTON_B, BUTTON_C, BUTTON_D,
    BUTTON_H_UP, BUTTON_H_DN, BUTTON_M_UP, BUTTON_M_DN,
    BUTTON_S_UP, BUTTON_S_DN, BUTTON_RESET, BUTTON_STARTSTOP,
    BUTTON_TIMER1, BUTTON_TIMER2};
const uint8_t button_count = sizeof(buttons) / sizeof(uint8_t);
bool button_states_now[button_count], button_states_before[button_count];
uint32_t preset_press_time[4];  // Press time of the 4 preset buttons, to detect holds
bool preset_held[4];  // Store preset hold state so it doesn't get repeatedly detected
const uint16_t button_hold_duration_ms = 3000;  // Time before a button hold is registered

// Some handy shortcuts
#define IS_PRESSED button_states_now[i]
#define WAS_PRESSED button_states_before[i]

// Display
U8G2_SSD1306_128X32_UNIVISION_1_HW_I2C u8g2(U8G2_R2);


enum time_ranges {
  HOUR,
  MINUTE,
  SECOND
};

typedef struct time_s {
  uint8_t h;
  uint8_t m;
  uint8_t s;
} time;

// Max time is 99:59:59 = 99*3600+59*60+59 = 359999
#define TIME_MAX 359999
#define TIME_MIN 0

#define PIN_DEBUG_RX 11
#define PIN_DEBUG_TX 12
#define PIN_BUZZER 5

// Alarm pattern for beeping
// Starts with an off
// Max time for one beep/silence is (just over) 60s
uint32_t alarm_pattern_start_ms;
uint32_t alarm2_pattern_start_ms;
uint16_t alarm_pattern_ms[] = {1, 50, 100, 50, 100, 50, 100, 50, 500};
uint8_t alarm_pattern_len = sizeof(alarm_pattern_ms) / sizeof(uint16_t);
uint8_t alarm_cycles = 2;  // No. cycles before a longer pause
uint8_t alarm_pause_s = 20;  // Time before alarm cycles start again
uint8_t alarm_cycle_count = 0;  // Counts number of cycles actually done

// All times will be internally represented as a number of seconds
uint32_t timer_s, timer_end_ms, timer_delta_ms;
bool timer_running, timer_changed, alarm;
uint32_t timer2_s, timer2_end_ms, timer2_delta_ms;
bool timer2_running, timer2_changed, alarm2;
uint32_t current_time_ms;
uint8_t selected_timer = 1;
time preset_A, preset_B, preset_C, preset_D;


void setup() {
  initButtonPins();
  pinMode(PIN_BUZZER, OUTPUT);

  // Start display
  u8g2.begin();

  // Set preset values (only needed for first run)
  //preset_A = {0, 0, 30};
  //preset_B = {0, 2, 30};
  //preset_C = {0, 5, 0};
  //preset_D = {0, 30, 0};
  //savePresetToEEPROM(0);
  //savePresetToEEPROM(1);
  //savePresetToEEPROM(2);
  //savePresetToEEPROM(3);

  // Get preset values from EEPROM
  readPresetsFromEEPROM();

  // Set time on screen to 0
  updateDisplay();

  Serial.begin(115200);
  Serial.println("Initialised");
}


void loop() {
  // Scan through button matrix for changes
  for (uint8_t i=0; i<button_count; i++) {
    // Invert button reading so HIGH means pressed
    button_states_now[i] = !digitalRead(buttons[i]);

    if (IS_PRESSED && !WAS_PRESSED) {
      // Freshly pressed
      doButtonPressAction(buttons[i]);
      updateDisplay();
    }
    else if (IS_PRESSED && WAS_PRESSED) {
      // Button held but maybe not freshly held - check that before doing anything
      if (isButtonFreshlyHeld(buttons[i])) {
        doButtonHoldAction(buttons[i]);
        updateDisplay();
      }
    }
    else if (WAS_PRESSED && !IS_PRESSED) {
      // Freshly released
      doButtonReleaseAction(buttons[i]);
      updateDisplay();
    }

    // Save button state to compare to next loop
    button_states_before[i] = button_states_now[i];
  }

  // Handle timer
  if (timer_changed) {
    if (timer_running) {
      // Timer just started
      timer_end_ms = millis() + timer_s * 1000;
      //Serial.print("b_startstop.txt=\"STOP\"\xFF\xFF\xFF");
    }
    else {
      // Timer just stopped
      //Serial.print("b_startstop.txt=\"START\"\xFF\xFF\xFF");
    }
    // Change handled, flag can be unset
    timer_changed = false;
  }

  else if (timer_running) {
    // Timer running but not freshly started
    current_time_ms = millis();
    timer_delta_ms = timer_end_ms - current_time_ms;
    timer_s = timer_delta_ms / 1000;
    updateDisplay();

    // If timer has just finished, start the alarm
    if (timer_s == 0) {
      timer_running = false;
      alarm = true;
      alarm_pattern_start_ms = current_time_ms;
      alarm_cycle_count = 0;
    }
  }
  else if (alarm) {
    // Timer has finished - sound alarm
    buzzerAlarm();
  }

  // Handle timer
  if (timer2_changed) {
    if (timer2_running) {
      // Timer just started
      timer2_end_ms = millis() + timer2_s * 1000;
      //Serial.print("b_startstop.txt=\"STOP\"\xFF\xFF\xFF");
    }
    else {
      // Timer just stopped
      //Serial.print("b_startstop.txt=\"START\"\xFF\xFF\xFF");
    }
    // Change handled, flag can be unset
    timer2_changed = false;
  }

  else if (timer2_running) {
    // Timer running but not freshly started
    current_time_ms = millis();
    timer2_delta_ms = timer2_end_ms - current_time_ms;
    timer2_s = timer2_delta_ms / 1000;
    updateDisplay();

    // If timer has just finished, start the alarm
    if (timer2_s == 0) {
      timer2_running = false;
      alarm = true;
      alarm2_pattern_start_ms = current_time_ms;
      alarm_cycle_count = 0;
    }
  }
  else if (alarm) {
    // Timer has finished - sound alarm
    buzzerAlarm();
  }
}


void initButtonPins(void) {
    for (uint8_t i=0; i<button_count; i++) {
        pinMode(buttons[i], INPUT_PULLUP);
    }
}


void doButtonPressAction(uint8_t button) {
  switch(button) {
    case BUTTON_A:
      savePressTime(button);
      break;
    case BUTTON_B:
      savePressTime(button);
      break;
    case BUTTON_C:
      savePressTime(button);
      break;
    case BUTTON_D:
      savePressTime(button);
      break;
    case BUTTON_H_UP:
      changeSingleValue(HOUR, 1);
      break;
    case BUTTON_H_DN:
      changeSingleValue(HOUR, -1);
      break;
    case BUTTON_M_UP:
      changeSingleValue(MINUTE, 1);
      break;
    case BUTTON_M_DN:
      changeSingleValue(MINUTE, -1);
      break;
    case BUTTON_S_UP:
      changeSingleValue(SECOND, 1);
      break;
    case BUTTON_S_DN:
      changeSingleValue(SECOND, -1);
      break;
    case BUTTON_RESET:
      if (selected_timer == 1) {
        timer_s = 0;
        timer_running = false;
        timer_changed = true;
        alarm = false;
      }
      else {
        timer2_s = 0;
        timer2_running = false;
        timer2_changed = true;
        alarm2 = false;
      }
      break;
    case BUTTON_STARTSTOP:
      if (selected_timer == 1) {
        if (timer_running) {
          timer_running = false;
        }
        else if (timer_s != 0) { // Don't start timer if it would be 0 seconds
          timer_running = true;
        }
        timer_changed = true;
        alarm = false;
      }
      else {
        if (timer2_running) {
          timer2_running = false;
        }
        else if (timer2_s != 0) { // Don't start timer if it would be 0 seconds
          timer2_running = true;
        }
        timer2_changed = true;
        alarm = false;
      }
      break;
    case BUTTON_TIMER1:
      selected_timer = 1;
      break;
    case BUTTON_TIMER2:
      selected_timer = 2;
      break;
    default:
      Serial.println("Wrong value passed to doButtonPressAction!");
    }
}


void doButtonHoldAction(uint8_t button) {

  // Get pointer for selected timer
  uint32_t * timer;
  if (selected_timer == 1) timer = &timer_s;
  else timer = &timer2_s;

  switch(button) {
    // Set preset to current timer value, limited to max of 0:59:59
    case BUTTON_A:
      preset_A = secondsToTime(min(*timer, 3599));
      savePresetToEEPROM(0);
      break;
    case BUTTON_B:
      preset_B = secondsToTime(min(*timer, 3599));
      savePresetToEEPROM(1);
      break;
    case BUTTON_C:
      preset_C = secondsToTime(min(*timer, 3599));
      savePresetToEEPROM(2);
      break;
    case BUTTON_D:
      preset_D = secondsToTime(min(*timer, 3599));
      savePresetToEEPROM(3);
      break;
    default:
      break;
  }
}


void doButtonReleaseAction(uint8_t button) {
  switch(button) {
    case BUTTON_A:
      preset_held[0] = false;
      addTime(preset_A);
      break;
    case BUTTON_B:
      preset_held[1] = false;
      addTime(preset_B);
      break;
    case BUTTON_C:
      preset_held[2] = false;
      addTime(preset_C);
      break;
    case BUTTON_D:
      preset_held[3] = false;
      addTime(preset_D);
      break;
    default:
      break;
  }
}


void savePressTime(uint8_t button) {
  /*
   *  Set specified button's press time to current time
   */

  switch(button) {
  case BUTTON_A:
    preset_press_time[0] = millis();
    break;
  case BUTTON_B:
    preset_press_time[1] = millis();
    break;
  case BUTTON_C:
    preset_press_time[2] = millis();
    break;
  case BUTTON_D:
    preset_press_time[3] = millis();
    break;
  }
}


bool isButtonFreshlyHeld(uint8_t button) {
  /*
   *  Check if specified button has been held for long enough to count as a press
   */

  uint8_t i;

  if (button == BUTTON_A) i=0;
  else if (button == BUTTON_B) i=1;
  else if (button == BUTTON_C) i=2;
  else if (button == BUTTON_D) i=3;
  else return 0; // Ignore other button presses

  // Ignore if it's already held, so we don't create extra hold presses
  if (preset_held[i]) return false;

  if (preset_press_time[i] + button_hold_duration_ms <= millis()) {
    preset_held[i] = true;
    return true;
  }
  else return false;
}


time secondsToTime(uint32_t seconds) {
  /*
   *  Convert a number of seconds to a time object with hours, minutes and seconds
   */

  uint8_t h, m, s;

  h = seconds / 3600;
  m = (seconds % 3600) / 60;
  s = seconds % 60;

  time combined = {h, m, s};

  return combined;
}


uint32_t timeToSeconds(time t) {
  /*
   *  Convert a time object to a number of seconds
   */

  uint32_t seconds = (uint32_t)3600*t.h + (uint32_t)60*t.m + (uint32_t)t.s;
  return seconds;
}


void printTimeToDisplay(uint8_t cursor_x, uint8_t cursor_y,
                        time time_to_print, bool print_hours) {
  /*
   *  Print a time to the display in the format HH:MM:SS
   *  If print_hours is false, just use the format MM:SS
   */

  u8g2.setCursor(cursor_x, cursor_y);

  if (print_hours) {
    u8g2.print(u8x8_u8toa(time_to_print.h, 2));
    u8g2.print(":");
  }
  u8g2.print(u8x8_u8toa(time_to_print.m, 2));
  u8g2.print(":");
  u8g2.print(u8x8_u8toa(time_to_print.s, 2));
}


void updateDisplay() {
  /*
   *  Update the displayed time from the timer_s global variable
   */

  time timer_time = secondsToTime(timer_s);
  time timer2_time = secondsToTime(timer2_s);

  //Serial.print(timer_time.h);
  //Serial.print(":");
  //Serial.print(timer_time.m);
  //Serial.print(":");
  //Serial.println(timer_time.s);

  u8g2.firstPage();
  do {
    // Smaller font, show presets in corners
    u8g2.setFont(u8g2_font_5x8_tf);
    printTimeToDisplay(0,    7, preset_A, false);
    printTimeToDisplay(103,  7, preset_B, false);
    printTimeToDisplay(0,   31, preset_C, false);
    printTimeToDisplay(103, 31, preset_D, false);

    // Larger font for timers
    u8g2.setFont(u8g2_font_8x13_tf);

    // Show timer 1
    //u8g2.setCursor(31, 14);
    printTimeToDisplay(31, 14, timer_time, true);

    // Show timer 2
    //u8g2.setCursor(31, 27);
    printTimeToDisplay(31, 27, timer2_time, true);

    // Mark selected timer with a rectangle
    if (selected_timer == 1) {
      u8g2.drawFrame(28, 3, 70, 13);
    }
    else {
      u8g2.drawFrame(28, 16, 70, 13);
    }

  } while (u8g2.nextPage());
}


uint32_t * getSelectedTimerPointer() {
  /*
   *  Get a pointer to the seconds counter of the currently selected timer
   */

  // Get pointer for selected timer
  uint32_t * timer;
  if (selected_timer == 1) timer = &timer_s;
  else timer = &timer2_s;

  return timer;
}


void changeSingleValue(uint8_t value_to_change, int8_t change) {
  /*
   *  Adjust the time on the currently selected timer,
   *  changing only seconds/minutes/hours and ignoring carry-over
   *  e.g. 02:59:59 + 1 -> 02:59:00
   */

  uint32_t * timer = getSelectedTimerPointer();

  time t = secondsToTime(*timer);

  // Add the change
  // Catch and prevent underflows and overflows
  if (value_to_change == SECOND) {
    if (change < 0 && abs(change) > t.s) t.s += change + 60;
    else if (change > 0 && change + t.s > 59) t.s += change - 60;
    else t.s += change;
  }
  else if (value_to_change == MINUTE) {
    if (change < 0 && abs(change) > t.m) t.m += change + 60;
    else if (change > 0 && change + t.m > 59) t.m += change - 60;
    else t.m += change;
  }
  else if (value_to_change == HOUR) {
    if (change < 0 && abs(change) > t.h) t.h += change + 100;
    else if (change > 0 && change + t.h > 99) t.h += change - 100;
    else t.h += change;
  }

  *timer = timeToSeconds(t);
}


void addTime(time t) {
  /*
   *  Add a given time to the selected timer, limiting the max value
   */

  uint32_t * timer = getSelectedTimerPointer();

  int32_t t_s = timeToSeconds(t);
  uint32_t t_added = *timer + t_s;

  // Check for overflow and limit to 99:59:59
  if (t_added > TIME_MAX) *timer = TIME_MAX;
  else if (t_added < TIME_MIN) *timer = TIME_MIN;
  else *timer = t_added;
}


void savePresetToEEPROM(uint8_t preset_num) {
  /*
   *  Save specified preset value to EEPROM
   *  Addresses:
   *    0x00: Preset A (0)
   *    0x03: Preset B (1)
   *    0x06: Preset C (2)
   *    0x09: Preset D (3)
   */

  switch(preset_num) {
    case 0:
      EEPROM.put(0x00, preset_A);
      break;
    case 1:
      EEPROM.put(0x03, preset_B);
      break;
    case 2:
      EEPROM.put(0x06, preset_C);
      break;
    case 3:
      EEPROM.put(0x09, preset_D);
      break;
  }
}


void readPresetsFromEEPROM(void) {
  /*
   *  Rear all presets from EEPROM to their proper variables
   *  Addresses:
   *    0x00: Preset A
   *    0x03: Preset B
   *    0x06: Preset C
   *    0x09: Preset D
   */
  EEPROM.get(0x00, preset_A);
  EEPROM.get(0x03, preset_B);
  EEPROM.get(0x06, preset_C);
  EEPROM.get(0x09, preset_D);
}


void buzzerBeep() {
  digitalWrite(PIN_BUZZER, HIGH);
  delay(100);
  digitalWrite(PIN_BUZZER, LOW);
  delay(50);
}


void buzzerAlarm() {
  /*
   *  Set buzzer to be on/off depending on where we are in alarm cycle
   */

  bool buzzer_on = false; // Pattern starts with buzzer off
  uint32_t alarm_next_ms = alarm_pattern_start_ms;

  current_time_ms = millis();

  // Go through alarm pattern until right section is found
  for (int i=0; i<alarm_pattern_len; i++) {
    alarm_next_ms += alarm_pattern_ms[i];

    // Beep if it's time to beep
    if (alarm_next_ms > current_time_ms) {
      if (buzzer_on) tone(PIN_BUZZER, 4000);
      else noTone(PIN_BUZZER);
      return;
    }
    buzzer_on = !buzzer_on;
  }

  // Return didn't happen - we must be on the next cycle
  if (++alarm_cycle_count < alarm_cycles) { // This isn't the last cycle - start the next immediately
    alarm_pattern_start_ms = current_time_ms;
  }
  else { // This was the last cycle - set the next one to start at the right point
    alarm_pattern_start_ms = current_time_ms + 1000*alarm_pause_s;
    alarm_cycle_count = 0;
  }
}
